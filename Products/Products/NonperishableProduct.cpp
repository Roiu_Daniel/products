#include "NonperishableProduct.h"

NonPerishableProduct::NonPerishableProduct(uint16_t id, const std::string& name, float rawPrice, NonPerishableProductType type):
	Product(id,name,rawPrice), m_type(type)
{
}

NonPerishableProductType NonPerishableProduct::GetType()
{
	return m_type;
}

uint8_t NonPerishableProduct::GetVat()
{
	return 19;
}

float NonPerishableProduct::GetPrice()
{
	return m_rawPrice + (19 * m_rawPrice) / 100;
}
