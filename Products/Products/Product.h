#pragma once
#include <cstdint>
#include<string>
#include<iostream>
#include "IPriceable.h"
class Product: public IPriceable
{
public:
	Product(uint16_t id, const std::string& name, float rawPrice);
	uint16_t GetId();
	std::string GetName();
	float GetRawPrice();
protected:
	uint16_t m_id;
	std::string m_name;
	float m_rawPrice;
};

