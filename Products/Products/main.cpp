#include "Product.h"
#include <fstream>
#include<iostream>
#include <algorithm>
#include<vector>

int main()
{
	std::ifstream productsFile("products.proddb");
	std::vector <Product> products;
	uint16_t id;
	std::string name;
	float price;
	uint16_t vat;
	std::string dateOrTipe;
	uint8_t number = 0;
	while (!productsFile.eof())
	{
		productsFile >> id >> name >> price >> vat >> dateOrTipe;
		products.push_back(Product(id, name, price, vat, dateOrTipe));
	}
	return 0;
}