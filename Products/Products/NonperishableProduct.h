#pragma once
#include"Product.h"
class NonPerishableProduct :public Product
{
public:
	NonPerishableProduct(uint16_t id, const std::string& name, float rawPrice, NonPerishableProductType type);
	NonPerishableProductType GetType();
	uint8_t GetVat();
	float GetPrice();
private:
	NonPerishableProductType m_type;
};
enum class NonPerishableProductType
{
	Clothing,
	SmallApliences,
	PersonalHygiene
};
