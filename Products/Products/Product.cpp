#include "Product.h"

Product::Product(uint16_t id, const std::string& name, float rawPrice):
	m_id(id),m_name(name),m_rawPrice(rawPrice)
{
}
uint16_t Product::GetId()
{
	return m_id;
}

std::string Product::GetName()
{
	return m_name;
}

float Product::GetRawPrice()
{
	return m_rawPrice;
}
