#pragma once
#include<iostream>
class IPriceable
{
	virtual uint8_t GetVat() = 0;
	virtual float GetPrice() = 0;
};
