#pragma once
#include "Product.h"
class PerishableProduct :public Product
{
public:
	PerishableProduct(uint16_t id, const std::string& name, float rawPrice, const std::string& expirationDate);
	std::string GetExpirationDate();
	uint8_t GetId();

private:
	std::string m_expirationDate;
};
