#include "PerishableProduct.h"

PerishableProduct::PerishableProduct(uint16_t id, const std::string& name, float rawPrice, const std::string& expirationDate):
	m_id(id),m_name(name), m_rawPrice(rawPrice),m_expirationDate(expirationDate)
{
}

uint8_t PerishableProduct::GetVat()
{
	return 9;
}

float PerishableProduct::GetPrice()
{
	return m_rawPrice + (9 * m_rawPrice) / 100;
}
